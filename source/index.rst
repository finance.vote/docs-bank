.. bank.vote documentation master file, created by
   sphinx-quickstart on Fri Jun 25 13:46:31 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bank.vote's documentation!
=====================================
`bank.vote`_ provides DAOs with secure scalable infrastructure for token vesting. Token recipients receive tokens over a period of time at a fixed rate per second, improving the incentive structure for DeFi projects. This can be used for investors, founders, employees, and contractors.

.. _bank.vote: https://bank.factorydao.xyz/#/

.. image:: ./images/bank_hz_b.png
   :height: 50px
   :alt: docs-bank missing
   :align: center
   :target: https://bank.factorydao.xyz/#/

.. toctree::
   :maxdepth: 2
   :caption: Contents:




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Our products
===============

.. |A| image:: ./images/launch_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/launch/en/latest/
   
.. |I| image:: ./images/influence_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/influence/en/latest/

.. |M| image:: ./images/markets_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/markets/en/latest/

.. |Y| image:: ./images/yield_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/yield/en/latest/

.. |MI| image:: ./images/mint_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/mint/en/latest/ 

* |A| `launch <https://financevote.readthedocs.io/projects/launch/en/latest/>`_
* |I| `influence <https://financevote.readthedocs.io/projects/influence/en/latest/>`_
* |M| `markets <https://financevote.readthedocs.io/projects/markets/en/latest/>`_
* |MI| `mint <https://financevote.readthedocs.io/projects/mint/en/latest/>`_
* |Y| `yield <https://financevote.readthedocs.io/projects/yield/en/latest/>`_
  
Back to main page
------------------
.. |H| image:: ./images/financevote_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/en/latest/

* |H| `Home <https://financevote.readthedocs.io/en/latest/>`_
